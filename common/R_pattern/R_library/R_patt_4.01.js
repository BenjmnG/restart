function R_patt_401 (){

    x = -99;
    y = -100;
    var spacing = 33;
    var staging = 50;

    for(var i = 0; i < 200; i += 1){

        frag.addClass('rotate').size('100%', '200%')
		var g = frag.group().addClass('a1 401_line');

        g.line(0, 80, 80, 0).attr({stroke: Rcolor, 'stroke-width': Rstroke, fill:'none'}).addClass('line');
        g.move(x, y );
        g.rotate(Rangle90);
        g.scale(Rscale ,Rscale);

		x = x + spacing;

        if (x > (fragwidth * 1.5)) {
        	
		    if(staging_count % 2 === 1) {	//Each odd stage, do an offset 
				x = (spacing *-3);
		    } else {
				x = (spacing *-2);
		    };

        staging_count = staging_count + 1
        y  = y  + staging;
        
        if (y  > fragheight + (staging * 15)) {
			return;
		    };
        };
    };
};