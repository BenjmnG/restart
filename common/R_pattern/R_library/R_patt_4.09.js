function R_patt_409 (){

    var spacing = 27;
    var staging = 90;

    for(var i = 0; i < 800; i += 1){

    	frag.size('300%', '300%').attr({'style': 'transform: translate(-50%, -50%) rotate('+Rangle45+'deg) !important;'});

		var g = frag.group().addClass('a1 409_elastic');

        g.path("M13.4,21.9c-1.2,4.7-2.5,6.4-4,6.1c-3-0.6,0.9-13,2.4-17S16.3-1.1,12,0.8S5.7,16.3,4.5,21.9 c-1,4.8-2.5,6.4-4,6.1")
        .attr({stroke: Rcolor, 'stroke-width': Rstroke/2, fill:'none'})
        .addClass('line');
        g.move(x, y );
        g.scale(Rscale*2);

		x = x + spacing;

        if (x > (fragwidth * 2.5)) {
        	
		    if(staging_count % 2 === 1) {	//Each odd stage, do an offset 
				x = spacing * 1.2;
		    } else {
				x = 0;
		    };

        staging_count = staging_count + 1
        y  = y  + staging;
        
        if (y  > fragheight * 3) {
			return;
		    };
        };
    };
};

