function R_patt_404(){
	
	var XOrigin = fragwidth / 2; //Point central X
	var YOrigin = fragheight / 2; //Point central Y
	
	var x = XOrigin ;
	var y = YOrigin / 2;

	var spacing = 32;
	var staging = 32;
	var diameter = 10; 



    for(var i = 0; i < 1000; i += 1){
    	
		var g = frag.group().addClass('a1 404_circle');

		g.ellipse(diameter, diameter)
		.attr({stroke: Rcolor, 'stroke-width': Rstroke, fill:'none'})
		.addClass('line');

        //g.move(x, y );
        g.cx(x);
        g.cy(y);
        g.scale(Rscale ,Rscale);

		diameter = diameter + 20;
		
/*        if (diameter > fragwidth * 1.2) {
        	
		    if(staging_count % 2 === 1) {	//Each odd stage, do an offset 
				x = spacing / 2;
		    } else {
				x = 0;
		    };

        staging_count = staging_count + 1
        y  = y  + staging;
*/
        if (diameter  > (fragheight * 3)) {
			return;
		    };
        	        
        };
    /*};*/
};		