function R_patt_402 (){

    x = - 80;
    y = -80;

    var spacing = 80;
    var staging = 40;

    for(var i = 0; i < 200; i += 1){

		var g = frag.group().addClass('a1 402_crossed');

        g.line(0, 80, 80, 0).attr({stroke: Rcolor, 'stroke-width': Rstroke, fill:'none'}).rotate(0).addClass('line');
        g.line(0, 80, 80, 0).attr({stroke: Rcolor, 'stroke-width': Rstroke, fill:'none'}).rotate(-90).addClass('line');
        g.move(x, y );
        g.scale(Rscale ,Rscale);

		x = x + spacing;

        if (x > (fragwidth * 1.5)) {
        	
            if(staging_count % 2 === 1) {   //Each odd stage, do an offset 
                x = spacing * -2;
            } else {
                x = spacing * -4;
            };

        staging_count = staging_count + 1
        y  = y  + staging;
        
        if (y  > fragheight + (staging * 5)) {
			return;
		    };
        };
    };
};