function R_patt_405(){

	x0 = ['0.25','1.25',];  // Understanding Canvas extra width
	x1 = x0[Math.floor(Math.random() * x0.length)]; // get random multiple for x

	y0 = ['1','0',];  // Y is linked to X because we need a perfect center.
	y1 = y0[Math.floor(Math.random() * y0.length)];
	
	var XOrigin = fragwidth / 2; //Point central X
	var YOrigin = fragheight / 2; //Point central Y
	
	var x = XOrigin * x1;
	var y = XOrigin * y1; 
	var spacing = 32;
	var staging = 32;
	var diameter = 10; 

    for(var i = 0; i < 1000; i += 1){
    	
		var g = frag.group().addClass('a1 405_circle_corner');

		g.ellipse(diameter, diameter)
		.attr({stroke: Rcolor, 'stroke-width': Rstroke, fill:'none'})
		.addClass('line');

        //g.move(x, y );
        g.cx(x);
        g.cy(y);
        g.scale(Rscale ,Rscale);

		diameter = diameter + 20;
		
/*        if (diameter > fragwidth * 1.2) {
        	
		    if(staging_count % 2 === 1) {	//Each odd stage, do an offset 
				x = spacing / 2;
		    } else {
				x = 0;
		    };

        staging_count = staging_count + 1
        y  = y  + staging;
*/
        if (diameter  > (fragheight * 5)) { //if offset --> increase
			return;
		    };
        	        
        };
    /*};*/

};		