function R_patt_301 (){

        var y  = -60;
        var spacing = 105;
        var staging = 0;

        var scale = 3;
        //var flip = scale;

    for(var i = 0; i < 200; i += 1){

		var g = frag.group().addClass('a1');

        g.polygon('2.5,15 32.5,5 35,2.5 32.5,0 2.5,10 0,12.5')
        .attr({fill: Rcolor}).addClass('fill');
        g.polygon('2.5,20 32.5,10 30.1,7.5 32.5,5 2.5,15 5,17.5')
        .attr({fill: '#fff'}).addClass('mask').move(2.5, 10 );

        //g.viewBox(0, 0, 35, 20)
        g.move(x, y );
        g.scale(scale);

		x = x + spacing;

        if (x > fragwidth) {
        staging_count = staging_count + 1
		    if(staging_count % 2 === 1) {	//Each odd stage, do an offset 
				//x = spacing / 2;
                x = 0
                y  = y  + staging;

		    } else {
				x = 0;
                y  = y  + ( 20 );

		    };

        y  = y  + staging;
        
        if (y  > fragheight + (staging * 1)) {
			return;
		    };
        };
    };
};

