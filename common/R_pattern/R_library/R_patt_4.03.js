function R_patt_403(){
	
	var y  = -8;
	var spacing = 32;
	var staging = 24;

    for(var i = 0; i < 1000; i += 1){
    	
		var g = frag.group().addClass('a1 403_dot');

		g.ellipse(9, 9)
		.addClass('fill');
        g.move(x, y );
        g.scale(Rscale ,Rscale);

		x = x + spacing;
		
        if (x > fragwidth * 1.5) {
        	
		    if(staging_count % 2 === 1) {	//Each odd stage, do an offset 
				x = spacing / 2;
		    } else {
				x = 0;
		    };

        staging_count = staging_count + 1
        y  = y  + staging;

        if (y  > fragheight + (staging * 1)) {
			return;
		    };
        	        
        };
    };
};		