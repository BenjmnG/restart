function R_patt_407 (){
    var spacing = 35;
    var staging = 75;

    for(var i = 0; i < 200; i += 1){

        //frag.size('200%', '200%').attr({'style': 'transform: translate(-60%, -25%) rotate('+Rangle45+'deg) !important;'});

		var g = frag.group().addClass('a1 407_mending');

        g.path("M1,25.5c0,0-0.7,5.5,5.4,5.5s7.2-3.1,6.6-5c-1.4-4.8-6.6-11.4-6.6-19.5C6.5,5.2,6.7,1,10.6,1 c4,0,4.1,4.2,4.1,5.5c0,8.2-5.2,14.8-6.6,19.5c-0.5,1.9,0.5,5,6.6,5s5.4-5.5,5.4-5.5")
        .attr({stroke: Rcolor, 'stroke-width': Rstroke/2, fill:'none'})
        .addClass('line');
        g.move(x, y );
        //g.rotate(Rangle45);
        g.scale(Rscale*2);

		x = x + spacing;

        if (x > (fragwidth * 2)) {
        	
		    if(staging_count % 2 === 1) {	//Each odd stage, do an offset 
				x = spacing / 2;
		    } else {
				x = 0;
		    };

        staging_count = staging_count + 1
        y  = y  + staging;
        
        if (y  > fragheight + (staging * 5)) {
			return;
		    };
        };
    };
};
