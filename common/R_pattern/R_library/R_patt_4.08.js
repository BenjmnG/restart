function R_patt_408 (){

    var spacing = 40;
    var staging = 181;

    for(var i = 0; i < 800; i += 1){

    	frag.addClass('rotate').size('200%', '300%').attr({'style': 'transform: translate(-55%, -50%) rotate('+Rangle45+'deg) !important;'}); //70, 20% ?

		var g = frag.group().addClass('a1 408_sanddown');

        g.polyline("1.4,60.1 4.7,0.1 7.7,60.1 10.9,0.1 13.9,60.1 ")
        .attr({stroke: Rcolor, 'stroke-width': Rstroke/2, fill:'none'})
        .addClass('line');
        g.move(x, y );
        g.scale(Rscale*2);

		x = x + spacing;

        if (x > (fragwidth * 2)) {
        	
		    if(staging_count % 2 === 1) {	//Each odd stage, do an offset 
				x = spacing / 2;
		    } else {
				x = 0;
		    };

        staging_count = staging_count + 1
        y  = y  + staging;
        
        if (y  > fragheight * 3) {
			return;
		    };
        };
    };
};


 