#Filter

There is one svg filter.
An SVG filter is used to add special effects to SVG graphics or other HTML element like a text or an image.

##How to use it

It's very simple. Paste the following code in your HTML page.
At the end for example.
You can reand that the filter has a name. 
You can have multiple filter in the same SVG element. Just call their by their ID in the CSS part (see below).

````
	<svg id=" " style="visibility: hidden" data-name="Calque 1" xmlns="http://www.w3.org/2000/svg" width="0" height="0" viewBox="0 0 2 2">
		<title></title>
		<defs>
			<filter id="Filter-Illustration_03">
				<!-- Add huge disturtion -->
				<feTurbulence baseFrequency=".1" type="fractalNoise" numOctaves="0" seed="5"></feTurbulence>
				<feDisplacementMap in="SourceGraphic" xChannelSelector="R" yChannelSelector="B" scale="3" result="t1"></feDisplacementMap> 

				<feComponentTransfer result="main">
				</feComponentTransfer>
				<!-- Add detailed turbulence -->
				<feTurbulence baseFrequency="5" type="turbulence" numOctaves="0" seed="5"></feTurbulence><!-- Passe Haut -->

				<feDisplacementMap in="main" scale="2" xChannelSelector="R" yChannelSelector="B" result="Textured_Text"></feDisplacementMap>
			</filter> 

		</defs>
	 </svg>

````

And the, don't forget to call it in your CSS file

````
		<!-- Special svg filter -->
		<style type="text/css">
			.filtered{
				filter: url(#Filter-Illustration_03);
				-webkit-filter: url(#Filter-Illustration_03);
			}
		</style>
````

##Tips

To prevent a SVG interruption in your page, use ``visibility: hidden"`` instead of  ``display: none``.
