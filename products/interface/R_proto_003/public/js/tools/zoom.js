const zoom = document.querySelector('#ZoomRange');
var page = document.querySelector('#page');
var reset = document.querySelector('#ZoomReset');

$(document).ready( function() {
	zoom.value = 1;
})

zoom.addEventListener('mousemove', function() {
    console.log(zoom.value);
    page.style.setProperty('transform', 'scale(' + zoom.value + ')')
});

reset.addEventListener('click', function() {
	zoom.value = 1;
})