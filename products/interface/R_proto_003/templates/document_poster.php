<!DOCTYPE html>
<html style="--background-X:-35px; --background-Y:-1000px; --background-size:398px; --:undefined;" lang="en"><head>

	<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	    <meta charset="UTF-8">
	    <title>Poster</title>
	        <link rel="stylesheet" href="../public/css-compiled/theme.css" type="text/css">
		    <link rel="stylesheet" href="../public/css/introjs.css" type="text/css">

		    <link rel="stylesheet" href="../public/css-compiled/spectre.css">
			<link rel="stylesheet" href="../public/css-compiled/spectre-exp.css">

			<script src="../public/js/interact.js" type="text/javascript"></script>
	    	<script src="../public/js/html2canvas.js" type="text/javascript"></script>
						
			<style>
				@page {size: 210mm 297mm }

			</style></head>

	</head>
	<body>

	<div id="panel">
		<?php
			include 'tools/_page_poster.html';
			include 'tools/_background.html';
			include 'tools/_color.html';
			include 'tools/_export.html';
			include 'tools/_logo.html';
		?>
	</div>

	<div id="panel" class="right">
		<?php
			include 'tools/_zoom.html';
		?>
	</div>

    <div class="page" id="page">
      <div class="spread">
        <div class="background" style="background-image: url();"></div>
        <div class="poster " id="poster" data-targetsize="0.45">
          <hgroup id="drag-titles">
            <h1 class="draggable" id="booktitle" spellcheck="false" contenteditable="true">TITLE!</h1>
            <p class="draggable" spellcheck="false" contenteditable="true">Authors?</p>
          </hgroup>
        </div>
      </div>
    </div>

	</body>
		<!-- Script -->

		<script src="../public/js/jquery-3.x.min.js"></script>

	    <script src="../public/js/tools/helpers.js"></script>
	    <script src="../public/js/tools/ui.js"></script>
	    <script src="../public/js/tools/colors.js"></script>
	    <script src="../public/js/tools/export.js"></script>
	    <script src="../public/js/tools/content.js"></script>
	    <script src="../public/js/tools/logo.js"></script>
	    <script src="../public/js/tools/zoom.js"></script>
	    <script src="../public/js/tools/poster.js"></script>


</html>