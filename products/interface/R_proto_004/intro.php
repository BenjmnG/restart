<!-- Here, name Support and specefics tools needed -->
<?php
	$pageWidth = null;
	$pageHeight = null;

	$pageTitle = "Introduction"
?>

<!DOCTYPE html>
<html style="--background-X:-35px; --background-Y:-1000px; --background-size:398px; --:undefined;" lang="en">

	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="UTF-8">
		<title><?php  echo $pageTitle ?></title>

	    <link rel="stylesheet" href="public/css/introjs.css" type="text/css">
	    <link rel="stylesheet" href="public/css/introjs-modern.css" type="text/css">

	    <link rel="stylesheet" href="public/css-compiled/spectre.css">
		<link rel="stylesheet" href="public/css-compiled/spectre-icon.css">
		<link rel="stylesheet" href="public/css-compiled/spectre-exp.css">
	   
	    <link rel="stylesheet" href="public/css-compiled/theme.css" type="text/css">

		<script src="public/js/intro.min.js" type="text/javascript"></script>
	</head>

	<body id="introduction">

	<div class="notification">
		<div id="tutorial">
			<label class="form-switch">
		    	<input id="tutorialAction" type="checkbox">
		    	<i class="form-icon"></i>
		    	<?php include 'public/img/support.svg'; ?>
			</label>
		</div>
	</div>

	<div class="wilkommen">
		<h1>Welcome on the design automation tool. This tool will provide you basic interface for designing preset supports</h1>
	</div>

	<div class="document_list">

		<h2>What would you want to do:</h2>

    <?php
    $dir    = 'templates/';
	$files = glob("templates/document*.php");
	$Sentence = array("Make a", "Design a", "Edit a");


	foreach ($files as $fi){

		$RdS = array_rand($Sentence);

		$f = $fi;
		$f = preg_replace('/.php/', '', $f);
		$f = preg_replace('/templates\//', '', $f);
		$f = preg_replace('/document_/', '', $f);

		echo '<ul>
			<li>
				<a href="'.$fi.'">'.$Sentence[$RdS].' '.$f.'</a>
			</li>
		</ul>';
	}

	?>	
	</div>

	</body>
	<script src="public/js/jquery-3.x.min.js"></script>
</html>