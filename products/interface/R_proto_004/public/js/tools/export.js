// download front cover
  const ftcover = document.querySelector('.spread');
  const reload = document.querySelector('#generator');
  const dl = document.querySelector('#download');
  
  var spinecover = document.querySelector(".spinecover");
  var backcover = document.querySelector(".backcover");
  var frontcover = document.querySelector(".frontcover");

  reload.addEventListener('click', renderjpg)

  function renderjpg() {
    html2canvas(ftcover,  {
     onrendered: function(canvas) {
      dl.setAttribute('href',(canvas.toDataURL('image/jpeg')));
     }
    });
 
  }

  // html2canvas(document.getElementById('front'), {
  //     onrendered: function(canvas) {
  //      window.open(canvas.toDataURL('image/jpeg'));
  //    }
  // });





  // show/hide front 
  //const Xfront = document.querySelector('#Xfront')
  //const Xback = document.querySelector('#Xback')
  const sc = document.querySelector('#sc')
  const Xpage = document.querySelector('#Xpage')

  var page = document.querySelector('.document');

/*
  Xfront.addEventListener('click', function() {
    // hide spine
    document.querySelector('.spinecover').classList.toggle('hide');
    // hide back
    document.querySelector('.backcover').classList.toggle('hide');
    

    // remove crop marks
    document.querySelector('.spread').classList.toggle('nobefore');
    document.querySelector('.spread').classList.toggle('noafter');
    document.querySelector('.spread').classList.toggle('modif');
    // change frontcover location in the grid
    document.querySelector('.frontcover').classList.toggle('modif');
     // change position
    document.querySelector('.background').classList.toggle('modif');
    const pageWidth = parseInt(document.querySelector('#width').value, 10)
    const pageHeight = parseInt(document.querySelector('#height').value, 10);
    var style = document.createElement('style');
    style.innerHTML = '@page { size: ' + pageWidth + 'mm ' + pageHeight + 'mm ; margin: 0; padding: 0; }';
    document.head.appendChild(style);
    sc.classList.toggle('hide');
    Xfront.classList.toggle('hide');
    Xback.classList.toggle('hide');

  });
*/

  // spread view
  sc.addEventListener('click', function() {
    // hide spine
    if(spinecover){
      spinecover.classList.toggle('hide');
    }
    // hide back
    if(backcover){
      backcover.classList.toggle('hide');
    }

     // remove crop marks
    document.querySelector('.spread').classList.toggle('nobefore');
    document.querySelector('.spread').classList.toggle('noafter');
    // hide backcover
    document.querySelector('.spread').classList.toggle('modif');
    
    // change frontcover location in the grid
    if(frontcover){
      frontcover.classList.toggle('modif');
    }

     // change position
    document.querySelector('.background').classList.toggle('modif');
    const pageWidth = parseInt(document.querySelector('#width').value, 10)
    const pageHeight = parseInt(document.querySelector('#height').value, 10);
    var style = document.createElement('style');
    style.innerHTML = '@page { size: ' + pageWidth + 'mm ' + pageHeight + 'mm ; margin: 0; padding: 0; }';
    document.head.appendChild(style);
    sc.classList.toggle('hide');
    Xfront.classList.toggle('hide');
    Xback.classList.toggle('hide');

    pageSize()
  });


/*
  Xback.addEventListener('click', function() {
    // hide spine
    document.querySelector('.spinecover').classList.toggle('hide');
    // hide front
    document.querySelector('.frontcover').classList.toggle('hide');
    

     // remove crop marks
    document.querySelector('.spread').classList.toggle('nobefore');
    document.querySelector('.spread').classList.toggle('noafter');
    document.querySelector('.spread').classList.toggle('modif');

     // change position
    document.querySelector('.background').classList.toggle('modifback');
    const pageWidth = parseInt(document.querySelector('#width').value, 10)
    const pageHeight = parseInt(document.querySelector('#height').value, 10);
    var style = document.createElement('style');
    style.innerHTML = '@page { size: ' + pageWidth + 'mm ' + pageHeight + 'mm ; margin: 0; padding: 0; }';
    document.head.appendChild(style);
    // sc.classList.toggle('hide');
    Xfront.classList.toggle('hide');
    if (Xback.innerText === 'spread') {
      Xback.innerText = 'back';
    }
    else {
      Xback.innerText = 'spread';
    }
  });
*/
    Xpage.addEventListener('click', function() {

    // remove crop marks
    document.querySelector('.spread').classList.toggle('nobefore');
    document.querySelector('.spread').classList.toggle('noafter');
    document.querySelector('.spread').classList.toggle('modif');
    // change poster location in the grid
    document.querySelector('.document').classList.toggle('modif');
     // change position
    document.querySelector('.background').classList.toggle('modif');
    const pageWidth = parseInt(document.querySelector('#width').value, 10)
    const pageHeight = parseInt(document.querySelector('#height').value, 10);
    var style = document.createElement('style');
    style.innerHTML = '@page { size: ' + pageWidth + 'mm ' + pageHeight + 'mm ; margin: 0; padding: 0; }';
    document.head.appendChild(style);
    sc.classList.toggle('hide');

  });
