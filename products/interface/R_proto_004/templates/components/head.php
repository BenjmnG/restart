<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta charset="UTF-8">
	<title><?php  echo $pageTitle ?></title>

    <link rel="stylesheet" href="../public/css/introjs.css" type="text/css">
    <link rel="stylesheet" href="../public/css/introjs-modern.css" type="text/css">

    <link rel="stylesheet" href="../public/css-compiled/spectre.css">
	<link rel="stylesheet" href="../public/css-compiled/spectre-icon.css">
	<link rel="stylesheet" href="../public/css-compiled/spectre-exp.css">
   
    <link rel="stylesheet" href="../public/css-compiled/theme.css" type="text/css">

	<script src="../public/js/interact.js" type="text/javascript"></script>
	<script src="../public/js/html2canvas.js" type="text/javascript"></script>
	<script src="../public/js/intro.min.js" type="text/javascript"></script>

	<style>
		<?php  echo '@page {size:'.$pageWidth.' '.$pageHeight.'}' ?>
	</style>

</head>