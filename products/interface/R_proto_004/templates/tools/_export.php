<div id="export" class="tool buttons drag collapse">
	<div class="divider text-center" data-content="Export">
		<span >Export</span>
	</div>
	<div class="export">
		<div class="form-group">
		  	<label class="form-label">Export</label>

		<?php
	    foreach ($part as $p) {
	      echo 
	      '<label class="form-radio">
		    	<input type="radio" name="export" checked>
		    	<i id="X'.$p.'" class="form-icon"></i>'.$p.'
		  </label>';
	    }
	    ?>		  	

		  <label class="form-radio">
		    	<input type="radio" name="export">
		    	<i id="sc" class="form-icon"></i> Spread
		  </label>
		</div>
		<div class="btn-group">
			<button  class="btn btn-primary" id="generator">Generate a Jpg file</button>
			<a id="download" download="YourFileName.jpg"><button class="btn">And Download it</button></a>
		</div>
	</div>
</div>