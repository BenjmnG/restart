<div  id="colors" class="tool buttons drag collapse">
  <div class="divider text-center" data-content="Colors">
    <span>Colors</span>
  </div>

  <div class="colors">
    <div class="color" id="text-area">
      <label class="form-label" for="color-text">Text color</label>
      <input class="form-input" id="text" name="color-text" value="#333" type="color">
    </div>

    <?php
    foreach ($part as $p) {
      echo 
      '<div class="color" id="'.$p.'-area"> 
        <label class="form-label" for="color-'.$p.'">'.$p.' color</label>
        <input class="form-input" id="'.$p.'" name="color-'.$p.'" value="#fff" type="color">
      </div>';
    }
    ?>

    <div class="color" id="logo-back">
      <label class="form-label" for="color-back">Logo back color</label>
      <input class="form-input" id="front" name="color-logo-back" value="#000" type="color">
    </div>
       
    <button class="btn" id="colorMask">Tint filter</button>
  </div>
</div>