<div  id="zoom" class="tool buttons drag">
  <div class="divider text-center" data-content="Zoom In/Out">
    <span>Zoom in / Out</span>
  </div>
  <div class="zoom">
    <input class="slider" id="ZoomRange" name="navigation zoom" max="10" min=".4" step="any" value="1" type="range">
    <button class="btn btn-sm" id="ZoomReset">Reset</button>
  </div>
</div>