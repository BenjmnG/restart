<!-- Here, name Support and specefics tools needed -->
<?php
	$support = "poster"; // poster - cover
	$part = array("page"); // front - back - spine - page

	$pageWidth = "210mm";
	$pageHeight = "297mm";

	$pageTitle = "Poster"
?>

<!DOCTYPE html>
<html style="--background-X:-35px; --background-Y:-1000px; --background-size:398px; --:undefined;" lang="en">

	<?php include 'components/head.php'; ?>

	<body> 

	<div id="panel">
		<?php
			include 'tools/_page_'.$support.'.php';
			include 'tools/_graphic_box.php';
			include 'tools/_background.php';
			include 'tools/_color.php';
			include 'tools/_logo.php';
			include 'tools/_export.php';

		?>

	</div>

	<div id="panel" class="right">
		<?php include 'tools/_zoom.php'; ?>
	</div>

	<div class="notification">
		<?php include 'components/tutorial.php'; ?>
	</div>

    <div class="document" id="document" data-intro='Here is your document' data-step="1" data-position="top-left-aligned">
    	<?php include 'components/spread_page.php'; ?>
    </div>

	<div id="dont"><div class="in"><p>Your screen is too small to display this tool</p></div></div>

	</body>
		<!-- Script -->

		<script src="../public/js/jquery-3.x.min.js"></script>

	    <script src="../public/js/tools/helpers.js"></script>
	    <script src="../public/js/tools/ui.js"></script>
	    <script src="../public/js/tools/colors.js"></script>
	    <script src="../public/js/tools/export.js"></script>
	    <script src="../public/js/tools/content.js"></script>
	    <script src="../public/js/tools/logo.js"></script>
	    <script src="../public/js/tools/zoom.js"></script>
	    <script src="../public/js/tools/poster.js"></script>

</html>