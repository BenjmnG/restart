<?php
require_once '../engine/twig/autoload.php';

$loader = new Twig_Loader_Array(array(
    'index' => 'Hello {{ name }}!',
));
$twig = new Twig_Environment($loader);

echo $twig->render('index', array('name' => 'B'));

?>

{# First evaluate Base template, then verse this page#}
{% extends 'partials/base.html.twig' %} 

  <style>@page {size: 100mm 250mm }</style></head>

{% block title %}Poster{% endblock %}

{% block body %}
  {{ include 'tools/_page_poster.html' }}
{% endblock %}

{% block body %}
    <div class="page" id="page">
      <div class="spread">
        <div class="background" style="background-image: url();"></div>
        <div class="frontcover" id="front">
          <hgroup id="drag-titles">
            <h1 class="draggable" id="booktitle" spellcheck="false" contenteditable="true">TITLE!</h1>
            <p class="draggable" spellcheck="false" contenteditable="true">Authors?</p>
          </hgroup>
        </div>
      </div>
    </div>
{% endblock %}