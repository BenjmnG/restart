// download front cover
  const ftcover = document.querySelector('.spread');
  const reload = document.querySelector('#generator');
  const dl = document.querySelector('#download');
  
  reload.addEventListener('click', renderjpg)

  function renderjpg() {
    html2canvas(ftcover,  {
     onrendered: function(canvas) {
      
       dl.setAttribute('href',(canvas.toDataURL('image/jpeg')));
     }
    });
 
  }

  // html2canvas(document.getElementById('front'), {
  //     onrendered: function(canvas) {
  //      window.open(canvas.toDataURL('image/jpeg'));
  //    }
  // });





  // show/hide front 
  const fc = document.querySelector('#fc')
  const bc = document.querySelector('#bc')
  const sc = document.querySelector('#sc')

  var page = document.querySelector('.page');

  fc.addEventListener('click', function() {
    // hide spine
    document.querySelector('.spinecover').classList.toggle('hide');
    // hide back
    document.querySelector('.backcover').classList.toggle('hide');
    

     // remove crop marks
    document.querySelector('.spread').classList.toggle('nobefore');
    document.querySelector('.spread').classList.toggle('noafter');
    document.querySelector('.spread').classList.toggle('modif');
    // change frontcover location in the grid
    document.querySelector('.frontcover').classList.toggle('modif');
     // change position
    document.querySelector('.background').classList.toggle('modif');
    const pageWidth = parseInt(document.querySelector('#width').value, 10)
    const pageHeight = parseInt(document.querySelector('#height').value, 10);
    var style = document.createElement('style');
    style.innerHTML = '@page { size: ' + pageWidth + 'mm ' + pageHeight + 'mm ; margin: 0; padding: 0; }';
    document.head.appendChild(style);
    sc.classList.toggle('hide');
    fc.classList.toggle('hide');
    bc.classList.toggle('hide');

  });


  // spread view

  sc.addEventListener('click', function() {
    // hide spine
    document.querySelector('.spinecover').classList.toggle('hide');
    // hide back
    document.querySelector('.backcover').classList.toggle('hide');
    

     // remove crop marks
    document.querySelector('.spread').classList.toggle('nobefore');
    document.querySelector('.spread').classList.toggle('noafter');
    // hide backcover
    document.querySelector('.spread').classList.toggle('modif');
    // change frontcover location in the grid
    document.querySelector('.frontcover').classList.toggle('modif');
     // change position
    document.querySelector('.background').classList.toggle('modif');
    const pageWidth = parseInt(document.querySelector('#width').value, 10)
    const pageHeight = parseInt(document.querySelector('#height').value, 10);
    var style = document.createElement('style');
    style.innerHTML = '@page { size: ' + pageWidth + 'mm ' + pageHeight + 'mm ; margin: 0; padding: 0; }';
    document.head.appendChild(style);
    sc.classList.toggle('hide');
    fc.classList.toggle('hide');
    bc.classList.toggle('hide');

    pageSize()
  });



  bc.addEventListener('click', function() {
    // hide spine
    document.querySelector('.spinecover').classList.toggle('hide');
    // hide front
    document.querySelector('.frontcover').classList.toggle('hide');
    

     // remove crop marks
    document.querySelector('.spread').classList.toggle('nobefore');
    document.querySelector('.spread').classList.toggle('noafter');
    document.querySelector('.spread').classList.toggle('modif');

     // change position
    document.querySelector('.background').classList.toggle('modifback');
    const pageWidth = parseInt(document.querySelector('#width').value, 10)
    const pageHeight = parseInt(document.querySelector('#height').value, 10);
    var style = document.createElement('style');
    style.innerHTML = '@page { size: ' + pageWidth + 'mm ' + pageHeight + 'mm ; margin: 0; padding: 0; }';
    document.head.appendChild(style);
    // sc.classList.toggle('hide');
    fc.classList.toggle('hide');
    if (bc.innerText === 'spread') {
      bc.innerText = 'back';
    }
    else {
      bc.innerText = 'spread';
    }
  });
