// window on load -> get the content of all value from the ui


const posX = document.querySelector('#imgX'),
      posY = document.querySelector('#imgY'),
      size = document.querySelector('#imgSize'),
      pattern = document.querySelector('#pattern');
var bodyStyles = window.getComputedStyle(document.body);
window.onload = start();

function start() {

console.log(posX.value);
document.documentElement.style.setProperty('--background-X', posX.value + 'px')
console.log(posY.value);
document.documentElement.style.setProperty('--background-Y', posY.value + 'px')
console.log(size.value);
document.documentElement.style.setProperty('--background-size', size.value + 'px')
pageSize();
}


// printing page size depending on bleed and white space around

const sizes = document.querySelectorAll('.buttons .size');
sizes.forEach(size => size.addEventListener('change', handleUpdate));




sizes.forEach(size => size.addEventListener('change', pageSize));

function pageSize() {
    const pageWidth = parseInt(document.querySelector('#width').value, 10)
    const spineWidth = parseInt(document.querySelector('#spine').value, 10);
    const pageHeight = parseInt(document.querySelector('#height').value, 10);
    var cssPagedMedia = (function() {
        var style = document.createElement('style');
        document.head.appendChild(style);
        return function(rule) {
            style.innerHTML = rule;
        };
    }());

    cssPagedMedia.size = function(size) {
        cssPagedMedia('@page {size: ' + (pageWidth * 2 + spineWidth + 40) + 'mm ' + (pageHeight + 40) + 'mm }');
    };

    cssPagedMedia.size()
}




// clone title form fromt to spine
//- clone the title on the spine cover
const title = document.querySelector("#booktitle");
const spinetitle = document.querySelector("#spinetitle");

title.addEventListener('keyup', function() {
    const content = title.textContent;
    spinetitle.textContent = content;
})







// add illustration for the background
const illus = document.querySelector('#backimage');
console.log(illus);
var
    input = document.querySelector('#imgback'),
    image = document.querySelector('#back-demo'),
    cover = document.querySelector('.background');

input.addEventListener('drop', onFileInput);
input.addEventListener('change', onFileInput);

function onFileInput(e) {
    var
        reader = new FileReader(),
        files = e.dataTransfer ? e.dataTransfer.files : e.target.files,
        i = 0;
    reader.onload = onFileLoad;
    while (files[i]) reader.readAsDataURL(files[i++]);
}


function onFileLoad(e) {
    var data = e.target.result;

    //- image.src = data;
    cover.style.backgroundImage = ' url(' + data + ')'
}


// to remove background
const background = document.querySelector(".background");
const backbutton = document.querySelector('#hideBack');


backbutton.addEventListener('click', function(e) {

    background.setAttribute('style', 'background-image: url(none)');
})



// image location

posX.addEventListener('mousemove', function() {
    console.log(posX.value);
    document.documentElement.style.setProperty('--background-X', posX.value + 'px')
});
posY.addEventListener('mousemove', function() {
    console.log(posY.value);
    document.documentElement.style.setProperty('--background-Y', posY.value + 'px')
});
size.addEventListener('mousemove', function() {
    console.log(size.value);
    document.documentElement.style.setProperty('--background-size', size.value + 'px')
});
pattern.addEventListener('click', function() {
    document.querySelector('.background').classList.toggle('repeat');

});