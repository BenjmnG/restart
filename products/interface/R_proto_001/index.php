<?php
require_once 'engine/twig/autoload.php';

$loader = new Twig_Loader_Filesystem('content');
$twig = new Twig_Environment($loader, array('cache' => false));


echo $twig->render('content/poster.html.twig');
?>

