<?php

# let people know if they are running an unsupported version of PHP
if(phpversion() < 7) {

  die('<h3>App requires PHP/7 or higher.<br>You are currently running PHP/'.phpversion().'.</h3><p>You should contact your host to see if they can upgrade your version of PHP.</p>');

} else {
	require_once 'app/Twig/autoload.php';
	  # require config
  	require_once 'app/config.php';
}

$loader = new Twig_Loader_Filesystem('templates');
$twig = new Twig_Environment($loader);

echo $twig->render('intro.html.twig');

?>
