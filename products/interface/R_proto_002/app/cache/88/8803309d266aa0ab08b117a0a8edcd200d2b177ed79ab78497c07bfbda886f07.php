<?php

/* partials/base.html.twig */
class __TwigTemplate_4fc5113a5d759772ef1833d3e17c68b3ef32d0e62988bd9f241dc70de9924052 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'tools' => array($this, 'block_tools'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "
<!DOCTYPE html>
<html style=\"--background-X:-35px; --background-Y:-1000px; --background-size:398px; --:undefined;\" lang=\"en\">

\t<head>
\t<meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\">
\t    <meta charset=\"UTF-8\">
\t    <title>Demo for the icons</title>
        ";
        // line 10
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 14
        echo "
\t    <script src=\"../../engine/js/interact.js\" type=\"text/javascript\"></script>
\t    <script src=\"../../engine/js/html2canvas.js\" type=\"text/javascript\"></script>
\t</head>
\t<body>
            ";
        // line 19
        $this->displayBlock('tools', $context, $blocks);
        echo "\t

            ";
        // line 21
        $this->displayBlock('body', $context, $blocks);
        echo "\t

\t</body>

        ";
        // line 25
        $this->displayBlock('javascripts', $context, $blocks);
        // line 34
        echo "
</html>";
    }

    // line 10
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 11
        echo "\t        <link rel=\"stylesheet\" href=\"../../engine/css-compiled/theme.css\" type=\"text/css\">
\t\t    <link rel=\"stylesheet\" href=\"../../engine/css/introjs.css\" type=\"text/css\">
        ";
    }

    // line 19
    public function block_tools($context, array $blocks = array())
    {
    }

    // line 21
    public function block_body($context, array $blocks = array())
    {
    }

    // line 25
    public function block_javascripts($context, array $blocks = array())
    {
        // line 26
        echo "\t    <script src=\"../../engine/js/tools/helpers.js\"></script>
\t    <script src=\"../../engine/js/tools/ui.js\"></script>
\t    <script src=\"../../engine/js/tools/page.js\"></script>
\t    <script src=\"../../engine/js/tools/colors.js\"></script>
\t    <script src=\"../../engine/js/tools/export.js\"></script>
\t    <script src=\"../../engine/js/tools/content.js\"></script>
\t    <script src=\"../../engine/js/tools/logo.js\"></script>
\t    ";
    }

    public function getTemplateName()
    {
        return "partials/base.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  87 => 26,  84 => 25,  79 => 21,  74 => 19,  68 => 11,  65 => 10,  60 => 34,  58 => 25,  51 => 21,  46 => 19,  39 => 14,  37 => 10,  27 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "partials/base.html.twig", "E:\\WORK\\Serveur\\wamp-3\\www\\Restarter\\R_proto_002\\templates\\partials\\base.html.twig");
    }
}
