<?php

/* intro.html.twig */
class __TwigTemplate_fc71e507cb7aa7f9446c93ecd4d1c2950ce7c2220b7c21b2cbc3da612d49c07e extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 2
        $this->parent = $this->loadTemplate("partials/base.html.twig", "intro.html.twig", 2);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'tool' => array($this, 'block_tool'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "partials/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_head($context, array $blocks = array())
    {
        // line 5
        echo "  <style>@page {size: 100mm 250mm }</style>
";
    }

    // line 8
    public function block_title($context, array $blocks = array())
    {
        echo "Poster";
    }

    // line 10
    public function block_tool($context, array $blocks = array())
    {
        // line 11
        echo "  ";
        $this->loadTemplate("tools/_page_poster.html", "intro.html.twig", 11)->display($context);
    }

    // line 14
    public function block_body($context, array $blocks = array())
    {
        // line 15
        echo "    <div class=\"page\" id=\"page\">
      <div class=\"spread\">
        <div class=\"background\" style=\"background-image: url();\"></div>
        <div class=\"frontcover\" id=\"front\">
          <hgroup id=\"drag-titles\">
            <h1 class=\"draggable\" id=\"booktitle\" spellcheck=\"false\" contenteditable=\"true\">TITLE!</h1>
            <p class=\"draggable\" spellcheck=\"false\" contenteditable=\"true\">Authors?</p>
          </hgroup>
        </div>
      </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "intro.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 15,  57 => 14,  52 => 11,  49 => 10,  43 => 8,  38 => 5,  35 => 4,  15 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "intro.html.twig", "E:\\WORK\\Serveur\\wamp-3\\www\\Restarter\\R_proto_002\\templates\\intro.html.twig");
    }
}
