<?php
$Rcities = Array("London", "Manchester", "Barcelona", "Ísafjörður", "Tunis", "Aosta");
$Rcity = array_rand($Rcities);
?>

<html>
<head>
<meta charset="utf-8">
<title> Restarter emblem generator</title>
<!-- Calling Css -->
<link rel="stylesheet" type="text/css" href="lib/css-compiled/R_social_8.12.2.css">
<!-- Calling fonts -->
<link rel="stylesheet" type="text/css" href="../../common/font/Asap/stylesheet.css">
<link rel="stylesheet" type="text/css" href="../../common/font/Gaia/stylesheet.css">
<link rel="stylesheet" type="text/css" href="../../common/font/Patua/stylesheet.css">

<!-- Special svg filter -->
<style type="text/css">
 </style>

</head>
<body>
	<div id="page">
		<div id="illustration1">
			<div class="illubox stuff">

				<?php
				/*
				$input = array("R-illu_fan.svg", "R-illu_battery.svg", "R-illu_iron.svg", "R-illu_printer.svg", "R-illu_toaster.svg");
				$rand_keys = array_rand($input);
				echo '<img class="svg" src="../../common/img/illustration/svg/' . $input[$rand_keys] . '">';
				*/
				?>
			</div>
		</div>		

		<div class="" id="minicartel">

			<div class="logo main">
				<img class="svg" src="../../common/img/logo/Restart_symbol_white.svg">
			</div>

			<div id="title">
				<hr>
				<h1 contenteditable="true">Fix the <br>system</h1>
			</div>

		</div>

		<div id="message" contenteditable="true">
			<span class="border"></span>
			<p class="highlight">56<sup>%</sup></p>
			<p class="abstract" contenteditable="true">That's a number so come to one of our free community repair events, where our volunteer fixers will help you learn how to repair your broken or slow devices - and tackle the growing mountain of e-waste.</p>
		</div>

			<p class="logo network txt">
				<span class="R">restart</span>
				<span id="Rcity" class="Rcity" contenteditable="true"><?php echo $Rcities[$Rcity]?></span>
			</p>
	</div>

<button id="black2color">Black or Color</button>

<svg id=" " style="visibility: hidden" data-name="Calque 1" xmlns="http://www.w3.org/2000/svg" width="0" height="0" viewBox="0 0 2 2">
	<title></title>
	<defs>
        <filter id="Filter-Text_01" >
			<!-- First Hard distortion -->
        	<feTurbulence baseFrequency=".06" type="fractalNoise" numOctaves="0" seed="5" result="t1" />
			<feDisplacementMap in="SourceGraphic" in2="t1" xChannelSelector="R" yChannelSelector="B" scale="4" result="main" /> 
			<!-- Then details -->
			<feTurbulence in="main" type="turbulence" baseFrequency="0.265" numOctaves="5" data-filterId="3" />
			>
			<feDisplacementMap xChannelSelector="R" yChannelSelector="G" in="main" scale="02" />

		</filter>

        <filter id="Filter-Text_02" >
			<!-- Details -->
			<feTurbulence type="turbulence" baseFrequency="0.265" numOctaves="5" result="turbulence_3" data-filterId="3" /> 
			<feDisplacementMap xChannelSelector="R" yChannelSelector="G" in="SourceGraphic" in2="turbulence_3" scale="02" />
		</filter>

        <filter id="Filter-Text_03" >
			<!-- Details -->
			<feTurbulence type="turbulence" baseFrequency="0.165" numOctaves="5" result="turbulence_3" data-filterId="3" /> 
			<feDisplacementMap xChannelSelector="R" yChannelSelector="G" in="SourceGraphic" in2="turbulence_3" scale="01" />
		</filter>

		<filter id="Filter-Illustration_01">
			<!-- Add huge disturtion -->
			<feTurbulence baseFrequency=".5" type="fractalNoise" numOctaves="0" seed="5" />
			<feDisplacementMap in="SourceGraphic" xChannelSelector="R" yChannelSelector="B" scale="3" result="t1" /> 

			<feComponentTransfer result="main">
			<feFuncA type="gamma" amplitude="10" exponent="5"/>
			</feComponentTransfer>

			<feColorMatrix type="matrix" values="
			0 0 0 0 0 
		    0 0 0 0 0
		    0 0 0 0 0
		    0  0 0 0 0"/>

		    <feComposite operator="over" in="main"/>

			<!-- Add detailed turbulence -->
			<feTurbulence baseFrequency="5" type="turbulence" numOctaves="0" seed="5" /><!-- Passe Haut -->
			<feDisplacementMap in="main" scale="2" xChannelSelector="R" yChannelSelector="B" result="Textured_Text" />
			<feColorMatrix in="Textured_Text" type="matrix" values="0 0 0 0 0 
		    0 0 0 0 0
		    0 0 0 0 0
		    0 0 0 1 0"/><!-- Multiply -->
		</filter>

		<filter id="Filter-Illustration_02">
			<!-- Add huge disturtion -->
			<feTurbulence baseFrequency=".2" type="fractalNoise" numOctaves="0" seed="5" />
			<feDisplacementMap in="SourceGraphic" xChannelSelector="R" yChannelSelector="B" scale="3" result="t1" /> 

			<feComponentTransfer result="main">
			<feFuncA type="gamma" amplitude="10" exponent="5"/>
			</feComponentTransfer>

			<feColorMatrix type="matrix" values="
			0 0 0 0 0 
		    0 0 0 0 0
		    0 0 0 0 0
		    0  0 0 0 0"/>

		    <feComposite operator="over" in="main"/>

			<!-- Add detailed turbulence -->
			<feTurbulence baseFrequency="5" type="turbulence" numOctaves="0" seed="5" /><!-- Passe Haut -->
			<feDisplacementMap in="main" scale="2" xChannelSelector="R" yChannelSelector="B" result="Textured_Text" />
			<feColorMatrix in="Textured_Text" type="matrix" values="0 0 0 0 0 
		    0 0 0 0 0
		    0 0 0 0 0
		    0 0 0 1 0"/><!-- Multiply -->
		</filter>

		<filter id="Filter-Illustration_03">
			<!-- Add huge disturtion -->
			<feTurbulence baseFrequency=".1" type="fractalNoise" numOctaves="0" seed="5"></feTurbulence>
			<feDisplacementMap in="SourceGraphic" xChannelSelector="R" yChannelSelector="B" scale="3" result="t1"></feDisplacementMap> 

			<feComponentTransfer result="main">
			</feComponentTransfer>
			<!-- Add detailed turbulence -->
			<feTurbulence baseFrequency="5" type="turbulence" numOctaves="0" seed="5"></feTurbulence><!-- Passe Haut -->

			<feDisplacementMap in="main" scale="2" xChannelSelector="R" yChannelSelector="B" result="Textured_Text"></feDisplacementMap>
		</filter> 

	</defs>
 </svg>

</body>
<!-- Calling general -->
<script  type="text/javascript" src="../../common/js/jquery-3.x.min.js"></script>
<script  type="text/javascript" src="../../common/js/svg.min.js"></script>
<script  type="text/javascript" src="../../common/js/svg.filter.min.js"></script>

<!-- Calling pattern -->
<script  type="text/javascript" src="../../common/R_pattern/R_library/R_patt_4.01.js"></script>
<script  type="text/javascript" src="../../common/R_pattern/R_library/R_patt_4.02.js"></script>
<script  type="text/javascript" src="../../common/R_pattern/R_library/R_patt_4.03.js"></script>
<script  type="text/javascript" src="../../common/R_pattern/R_library/R_patt_4.04.js"></script>
<script  type="text/javascript" src="../../common/R_pattern/R_library/R_patt_4.05.js"></script>
<script  type="text/javascript" src="../../common/R_pattern/R_library/R_patt_4.06.js"></script>

<script  type="text/javascript" src="../../common/R_pattern/R_library/R_patt_4.07.js"></script>
<script  type="text/javascript" src="../../common/R_pattern/R_library/R_patt_4.08.js"></script>
<script  type="text/javascript" src="../../common/R_pattern/R_library/R_patt_4.09.js"></script>


<!-- Calling Module -->
<script  type="text/javascript" src="../../common/R_pattern/R_var_8.01.js"></script>
<script  type="text/javascript" src="../../common/R_pattern/R_color_7.01.js"></script>
<script  type="text/javascript" src="../../common/R_pattern/R_svg_inline_8.01.js"></script>
<script  type="text/javascript" src="../../common/R_pattern/R_bricabrac_letter_8.01.js"></script>

<!-- Calling Specific -->
<script  type="text/javascript" src="lib/js/R_social_8.11.js"></script>

</html>

