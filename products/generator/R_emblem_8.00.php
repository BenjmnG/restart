<html>
<head>
<meta charset="utf-8">
<title> Restarter emblem generator</title>
<!-- Calling Css -->
<link rel="stylesheet" type="text/css" href="lib/css-compiled/R_emblem_8.00.css">
<!-- Calling fonts -->
<link rel="stylesheet" type="text/css" href="../../common/font/Asap/stylesheet.css">

<!-- Special svg filter -->
<style type="text/css">
	.filtered{
		filter: url(#Filter-Illustration_01);
		-webkit-filter: url(#Filter-Illustration_01);
	}
</style>

</head>
<body>
	<div id="page">

		<div class="logo">
			<img class="" src="../../common/img/logo/Restart_symbol.svg">
		</div>

		<div class="ramdam intermede kred" id="ramdam1">
			<hr>
			<hr>
			<div id="frag0" class="frag emblem filtered">

			</div>
			<div id="frag1" class="frag emblem filtered">

			</div>
			<div id="frag2" class="frag emblem filtered">
				
			</div>
			<div id="frag3" class="frag emblem filtered">
				
			</div>

		</div>
	</div>

<button id="black2color">Black or Color</button>

<svg id="Filter-Illustration" style="visibility: hidden" data-name="Calque 1" xmlns="http://www.w3.org/2000/svg" width="0" height="0" viewBox="0 0 2 2">
	<title></title>
	<defs>
		<filter id="Filter-Illustration_01">
			<!-- Add huge disturtion -->
			<feTurbulence baseFrequency=".1" type="fractalNoise" numOctaves="0" seed="5" />
			<feDisplacementMap in="SourceGraphic" xChannelSelector="R" yChannelSelector="B" scale="3" result="t1" /> 

			<feComponentTransfer result="main">
			</feComponentTransfer>
			<!-- Add detailed turbulence -->
			<feTurbulence baseFrequency="5" type="turbulence" numOctaves="0" seed="5" /><!-- Passe Haut -->

			<feDisplacementMap in="main" scale="2" xChannelSelector="R" yChannelSelector="B" result="Textured_Text" />
		</filter>    	
	</defs>
 </svg>

</body>
<!-- Calling general -->
<script  type="text/javascript" src="../../common/js/jquery-3.x.min.js"></script>
<script  type="text/javascript" src="../../common/js/svg.min.js"></script>
<script  type="text/javascript" src="../../common/js/svg.filter.min.js"></script>

<!-- Calling pattern -->
<script  type="text/javascript" src="../../common/R_pattern/R_library/R_patt_4.01.js"></script>
<script  type="text/javascript" src="../../common/R_pattern/R_library/R_patt_4.02.js"></script>
<script  type="text/javascript" src="../../common/R_pattern/R_library/R_patt_4.03.js"></script>
<script  type="text/javascript" src="../../common/R_pattern/R_library/R_patt_4.04.js"></script>
<script  type="text/javascript" src="../../common/R_pattern/R_library/R_patt_4.05.js"></script>
<script  type="text/javascript" src="../../common/R_pattern/R_library/R_patt_4.06.js"></script>

<script  type="text/javascript" src="../../common/R_pattern/R_library/R_patt_4.07.js"></script>
<script  type="text/javascript" src="../../common/R_pattern/R_library/R_patt_4.08.js"></script>
<script  type="text/javascript" src="../../common/R_pattern/R_library/R_patt_4.09.js"></script>


<!-- Calling Module -->
<script  type="text/javascript" src="../../common/R_pattern/R_var_7.01.js"></script>
<script  type="text/javascript" src="../../common/R_pattern/R_color_7.01.js"></script>
<script  type="text/javascript" src="../../common/R_pattern/R_svg_inline_7.01.js"></script>

<!-- Calling Specific -->
<script  type="text/javascript" src="lib/js/R_emblem_8.00.js"></script>

</html>

