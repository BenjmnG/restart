//////////////////
	//create div frag 
/////////////////
maxmi = 3;
count = 1; 
for(var mi = 0; mi < maxmi; mi += 1){
	if(mi == 1){ // Si 2nd col 
		maxcol = 3;
		mincol = 25; // largeur min column
		col = 33.333333;
	} else{
		maxcol = 2;
		mincol = 40; // largeur min column
		col = 50;
	}

	var ramdam = document.getElementById('ramdam1');
	var fragC = document.createElement('div')
	fragC.id = "fragC" + mi; 
	ramdam.appendChild(fragC);
	/*
	let [n, total, m = n] = [100, 0];
	const [min, arr, range = min + min / 2] = [mincol, []];

	// Define splitting value in percent
	do {
	  let r = Math.random() * (range - min) + min; // random number in our range
	  n -= r; // subtract `min` from `n`
	  arr.push(n > min ? r : m - total); // push `r` or remainder 
	  total += arr[arr.length - 1]; // keep track of total
	} while (n > min);

	console.log(arr);
	*/
	// specific case for the first of div
	var nfrag = document.createElement('div');
	nfrag.id = 'frag' + count;
	nfrag.className = 'frag';
	nfrag.style.height = ( 100 / maxmi ) + "%";
	nfrag.style.width = "calc(" + col + "% - " + (maxcol *2) + "px )";
	fragC.appendChild(nfrag);

	count = count + 1;

	for(var i = 2; i < (maxcol + 1); i += 1){ // Jump to the second to the fourth

		var nfrag = document.createElement('div');
		nfrag.id = 'frag' + count;
		nfrag.className = 'frag';
		nfrag.style.height = ( 100 / maxmi ) + "%";
		nfrag.style.width = col + "%";
		fragC.appendChild(nfrag);

		count = count + 1;
	};

};
//////////////////
	//Pattern
/////////////////

function dotrame(){

	$(".frag").each(function (index, element) {
		
		index = index + 1;

		FIt = Math.floor(Math.random() * (10 - 1)+1); //Max + 1 qui ne sera pas pris en compte

		x = 0;
		y = -10;

		spacing = 80;
		staging = 80;
		staging_count = 1;
	
		frag = SVG('frag' + index).size('100%', '100%')/*.addClass('frag_e'+ idx).fill('#fff')*/;
		fragwidth = document.getElementById('frag' + index).offsetWidth;
		fragheight = document.getElementById('frag' + index).offsetHeight;

	// Function de dessin
		if(FIt == 1) {  			R_patt_401(); // simple line
		} else if  (FIt == 2 ){ 	R_patt_402(); // crossed line
		} else if  (FIt == 3 ){ 	R_patt_403(); // dot
		} else if  (FIt == 4 ){		R_patt_404(); // Circle
		} else if  (FIt == 5 ){  	R_patt_405(); // Circle corner	
	    } else if  (FIt == 6 ){   	R_patt_406(); // Polygon
	    } else if  (FIt == 7 ){    	R_patt_407(); // Mending
	    } else if  (FIt == 8 ){ 	R_patt_408(); // Sand Down
	    } else if  (FIt == 9 ){ 	R_patt_409(); // Elastic 
	    } else if  (FIt == 10 ){ //Pattern 
		};


	});
};


//////////////////
	//Random content
/////////////////

function replaceContent(){
	var numberArray = [];

	for(var i = 1; i <= ((maxmi * maxcol) + 1); i++){
	    numberArray.push(i);
	}
	
	console.log(numberArray);

	var Rcities = ["London", "Manchester", "Barcelona", "Ísafjörður", "Tunis", "Aosta" ]
	var Rcity = Rcities[Math.floor(Math.random() * Rcities.length)];

	var handArray = ["R-illu_hand_wrench.svg", "R-illu_hand_wrench_2.svg", "R-illu_hand_socketwrench.svg", "R-illu_hand_screwdriver_1.svg", "R-illu_hand_crimping.svg", "R-illu_hand_cutter.svg", "R-illu_hand_electricscrewdriver.svg", "R-illu_hand_fist.svg", "R-illu_hand_hi.svg", "R-illu_hand_perfect.svg", "R-illu_hand_pliers.svg", "R-illu_hand_screwdriver.svg", "R-illu_hand_victory.svg"];
	var Rhand = handArray[Math.floor(Math.random() * handArray.length)];
	var stuffArray = ["R-illu_ammeter.svg", "R-illu_cissors.svg", "R-illu_ammeter_2.svg", "R-illu_battery.svg", "R-illu_crimping.svg", "R-illu_cutter.svg", "R-illu_electricscrewdriver.svg", "R-illu_fan.svg", "R-illu_iron.svg", "R-illu_pliers.svg", "R-illu_pliers_open.svg", "R-illu_printer.svg", "R-illu_screwdriver.svg", "R-illu_socketwrench.svg", "R-illu_soldering.svg", "R-illu_toaster.svg", "R-illu_tweezers.svg", "R-illu_wrench.svg", "R-illu_wrench_2.svg", "R-illu_wrench_3.svg"];
	var Rstuff = stuffArray[Math.floor(Math.random() * stuffArray.length)];

	var contentarray = ['<div class="inbox"><h2 contenteditable="true">Fix the System</h2></div>', 
						'<div class="inbox"><h3 contenteditable="true"><span>Come & <br>join us<br></span>'+ Rcity + '<br>01.01.19</h3></div>', 
						'<div class="illubox hand"><img class="svg" src="../../common/img/illustration/svg/' + Rhand + '"></div>',
						'<div class="illubox stuff"><img class="svg" src="../../common/img/illustration/svg/' + Rstuff + '"></div>',
						]

	// Lock the middle case
	var contentmiddle = '<div class="inbox" ><div class="logo main"><img class="svg" src="../../common/img/logo/Restart_symbol.svg"></div><p class="logo txt"> <span class="R">restart</span><span id="Rcity" class="Rcity" contenteditable="true">' + Rcity + '</span></p></div>';
	document.getElementById('frag4').innerHTML = contentmiddle;
	numberArray.splice(3, 1);


	for(var ci = 0; ci < 4; ci += 1){

		var rci = numberArray[Math.floor(Math.random() * numberArray.length)];
		
		//var roll = numberArray.splice(rci, 1);
		var index = numberArray.indexOf(rci);
		if (index > 0) {
		  numberArray.splice(index, 1);
		}		
		console.log("now " + rci);
		console.log("still " + numberArray);
		document.getElementById('frag' + rci).innerHTML = contentarray[ci]; 
	};


};



//////////////////
	//css style
/////////////////

// Run Draw when DOM ready
SVG.on(document, 'DOMContentLoaded', function() {
	dotrame();
	replaceContent();
	bricabrac();
	getsvg();

   })
